package me.tade.quickboard.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class WhenPluginUpdateTextEvent extends Event {

	private static HandlerList handlers = new HandlerList();
	
	private Player p;
	private String text;
	
	public WhenPluginUpdateTextEvent(Player p, String text){
		this.p = p;
		this.text = text;
	}
	
	public Player getPlayer(){
		return p;
	}
	
	public String getText(){
		return text;
	}
	
	public String setText(String text){
		this.text = text;
		return text;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlersList() {
		return handlers;
	}

}

