package me.tade.quickboard.cmds;

import me.tade.quickboard.PlayerBoard;
import me.tade.quickboard.QuickBoard;
import me.tade.quickboard.ScoreboardInfo;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class QuickBoardCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		if(args.length == 0){
			sender.sendMessage("§7[§6QuickBoard§7] §7Welcome §6" + sender.getName() + " §7to the QuickBoard commands!");
			sender.sendMessage("§7[§6QuickBoard§7] §7To show all commands use §6/quickboard cmds");
			sender.sendMessage("§7[§6QuickBoard§7] §7Plugin by §6The_TadeSK§7, version: §6" + QuickBoard.instance.getDescription().getVersion());
			return true;
		}
		if(args[0].equalsIgnoreCase("cmds")){
			sender.sendMessage("§7[§6QuickBoard§7] Toggle scoreboard §6/quickboard toggle");
			sender.sendMessage("§7[§6QuickBoard§7] Reload config file §6/quickboard reload");
			sender.sendMessage("§7[§6QuickBoard§7] §6/quickboard set <Player> <permission(name)> - Toggle custom scoreboard with name(permission)");
		}
		if(args[0].equalsIgnoreCase("toggle")){
			if(!(sender instanceof Player)){
				sender.sendMessage(QuickBoard.instance.getConfig().getString("messages.noperms").replace("&", "§"));
				return true;
			}
			Player p = (Player) sender;
			String text = "";
			if(QuickBoard.instance.boards.containsKey(p)){
				QuickBoard.instance.boards.get(p).remove();
				text = QuickBoard.instance.getConfig().getString("messages.ontoggle.false");
			}else{
				text = QuickBoard.instance.getConfig().getString("messages.ontoggle.true");
				for(String s : QuickBoard.instance.info.keySet()){
					if(p.hasPermission(s)){
						if(QuickBoard.instance.boards.containsKey(p)){
							ScoreboardInfo in = QuickBoard.instance.info.get(s);
							QuickBoard.instance.boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
						}else{
							new PlayerBoard(p, QuickBoard.instance.info.get(s));
						}
						break;
					}
				}
			}
			p.sendMessage(text.replace("&", "§"));
		}
		if(args[0].equalsIgnoreCase("set")){
			if(!sender.hasPermission("quickboard.set")){
				sender.sendMessage(QuickBoard.instance.getConfig().getString("messages.noperms").replace("&", "§"));
				return false;
			}
			if(args.length != 3){
				sender.sendMessage("Bad syntax");
				return true;
			}
			Player p = (Player) Bukkit.getPlayer(args[1]);
			String perm = args[2];
			if(!QuickBoard.instance.info.containsKey(perm)){
				sender.sendMessage("Scoreboard doesn't exists!");
				return true;
			}
			if(QuickBoard.instance.boards.containsKey(p)){
				ScoreboardInfo in = QuickBoard.instance.info.get(perm);
				QuickBoard.instance.boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
			}else{
				new PlayerBoard(p, QuickBoard.instance.info.get(perm));
			}
		}
		if(args[0].equalsIgnoreCase("reload")){
			if(!sender.hasPermission("quickboard.reload")){
				sender.sendMessage(QuickBoard.instance.getConfig().getString("messages.noperms").replace("&", "§"));
				return true;
			}
			sender.sendMessage("§7[§6QuickBoard§7] §6Reloading config file");
			QuickBoard.instance.reloadConfig();
			File fo = new File(QuickBoard.instance.getDataFolder().getAbsolutePath() + "/scoreboards");
			QuickBoard.instance.loadScoreboards(fo);
			sender.sendMessage("§7[§6QuickBoard§7] §aConfig file reloaded");
			sender.sendMessage("§7[§6QuickBoard§7] §6Creating scoreboard for all players");
			if(!QuickBoard.instance.getConfig().getBoolean("scoreboard.onjoin.use")){
				sender.sendMessage("§7[§6QuickBoard§7] §cCreating scoreboard failed! Creating scoreboard when player join to server is cancelled!");
				return true;
			}
			for(Player p : Bukkit.getOnlinePlayers()){
				if(QuickBoard.instance.boards.containsKey(p)){
					QuickBoard.instance.boards.get(p).remove();
					QuickBoard.instance.boards.remove(p);
				}
				for(String s : QuickBoard.instance.info.keySet()){
					ScoreboardInfo in = QuickBoard.instance.info.get(s);
					if(in.enabledWorlds != null && in.enabledWorlds.contains(p.getWorld().getName())){
						if(p.hasPermission(s)){
							if(QuickBoard.instance.boards.containsKey(p)){
								QuickBoard.instance.boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
							}else{
								new PlayerBoard(p, QuickBoard.instance.info.get(s));
							}
							break;
						}
					}
				}
			}
			sender.sendMessage("§7[§6QuickBoard§7] §aScoreboard created for all");
		}
		return false;
	}

}
