package me.tade.quickboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;

public class ScoreboardInfo {
	
	public YamlConfiguration cfg;
	public String permission;
	public List<String> text = new ArrayList<>();
	public List<String> title = new ArrayList<>();
	public int titleUpdate = 2;
	public int textUpdate = 5;
	public List<String> enabledWorlds = new ArrayList<>();
	public HashMap<String, List<String>> changeText = new HashMap<>();
	public HashMap<String, Integer> changeTextInterval = new HashMap<>();

	public ScoreboardInfo(YamlConfiguration config, String permission){
		cfg = config;
		this.permission = permission;
		initialize();
	}
	
	protected void initialize(){
		text = cfg.getStringList("text");
		title = cfg.getStringList("title");
		titleUpdate = cfg.getInt("updater.title", 2);
		textUpdate = cfg.getInt("updater.text", 5);
		if(cfg.getConfigurationSection("changeableText") != null){
			for(String s : cfg.getConfigurationSection("changeableText").getKeys(false)){
				changeText.put(s, cfg.getStringList("changeableText." + s + ".text"));
				changeTextInterval.put(s, cfg.getInt("changeableText." + s + ".interval", 30));
			}
		}
		if(cfg.getStringList("enabledWorlds").isEmpty()){
			enabledWorlds = null;
		}else{
			enabledWorlds = cfg.getStringList("enabledWorlds");
		}
	}
}
