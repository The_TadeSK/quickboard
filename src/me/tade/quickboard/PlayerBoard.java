package me.tade.quickboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.clip.placeholderapi.PlaceholderAPI;
import me.tade.quickboard.events.PlayerReceiveBoardEvent;
import me.tade.quickboard.events.WhenPluginUpdateTextEvent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class PlayerBoard {

	public Scoreboard board;
	public Objective score;
	public Player p;

	public List<Team> teams = new ArrayList<>();
	public HashMap<Team, String> lot = new HashMap<>();
	public List<String> list = new ArrayList<>();
	public List<String> title = new ArrayList<>();
	public List<String> chlist = new ArrayList<>();
	public int updateTitle = 2;
	public int updateText = 5;
	public int titleTask;
	public int textTask;
	boolean ch = false;
	public ScoreboardInfo info;
	public HashMap<String, String> chanText = new HashMap<>();
	public HashMap<String, Integer> chanTextInt = new HashMap<>();
	public List<Integer> tasks = new ArrayList<>();

	int index = 15;
	int titleindex = 0;

	public PlayerBoard(Player p, ScoreboardInfo info) {
		this.p = p;
		list = info.text;
		title = info.title;
		updateTitle = info.titleUpdate;
		updateText = info.textUpdate;
		
		for(String s : info.changeText.keySet()){
			chanTextInt.put(s, 0);
			chanText.put(s, info.changeText.get(s).get(0));
		}
		
		this.info = info;
		
		PlayerReceiveBoardEvent event = new PlayerReceiveBoardEvent(p, list, title, this);
		Bukkit.getPluginManager().callEvent(event);
		if(!event.isCancelled()){
			startSetup(event);
		}
	}

	public PlayerBoard(Player p, List<String> text, List<String> title, int updateTitle, int updateText) {
		this.p = p;
		this.updateTitle = updateTitle;
		this.updateText = updateText;
		PlayerReceiveBoardEvent event = new PlayerReceiveBoardEvent(p, text, title, this);
		Bukkit.getPluginManager().callEvent(event);
		if(!event.isCancelled()){
			startSetup(event);
		}
	}

	@SuppressWarnings("deprecation")
	public void startSetup(final PlayerReceiveBoardEvent event){
		if(QuickBoard.instance.boards.containsKey(p)){
			QuickBoard.instance.boards.get(p).remove();
		}
		c();
		titleindex = event.getTitle().size();
		QuickBoard.instance.boards.put(p, this);
		QuickBoard.instance.allboards.add(this);

		buildScoreboard(event);

		setUpText(event.getText());
		
		updater();
	}

	public void buildScoreboard(PlayerReceiveBoardEvent event){
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		score = board.registerNewObjective("score", "dummy");
		score.setDisplaySlot(DisplaySlot.SIDEBAR);
		score.setDisplayName(event.getTitle().get(0));
		
		p.setScoreboard(board);	
	}

	@SuppressWarnings("deprecation")
	public void setUpText(final List<String> text){
		Bukkit.getScheduler().scheduleAsyncDelayedTask(QuickBoard.instance, new Runnable(){
			public void run(){
				for(String s : text){
					Team t = board.registerNewTeam("Team:" + index);
					String normal = s;
					OfflinePlayer op = Bukkit.getOfflinePlayer(chlist.get(index - 1));
					t.addPlayer(op);
					s = setHolders(s);
					WhenPluginUpdateTextEvent event2 = new WhenPluginUpdateTextEvent(p, s);
					Bukkit.getPluginManager().callEvent(event2);
					String[] ts = splitString(event2.getText());
					setPrefix(t, ts[0]);
					setSuffix(t, ts[1]);
					score.getScore(op).setScore(index);
					teams.add(t);
					lot.put(t, normal);
					index--;
				}
			}
		}, 0L);
	}

	public void c(){
		chlist.add("§1§r");
		chlist.add("§2§r");
		chlist.add("§3§r");
		chlist.add("§4§r");
		chlist.add("§5§r");
		chlist.add("§6§r");
		chlist.add("§7§r");
		chlist.add("§8§r");
		chlist.add("§9§r");
		chlist.add("§a§r");
		chlist.add("§c§r");
		chlist.add("§b§r");
		chlist.add("§d§r");
		chlist.add("§e§r");
		chlist.add("§f§r");
	}

	public void updateText() {
		if(ch){
			return;
		}

		for(Team t : teams){
			String s = lot.get(t);
			for(String a : info.changeText.keySet()){
				if(s.contains("{CH_" + a + "}")){
					s = s.replace("{CH_" + a + "}", "");
					s = s + chanText.get(a);
				}
			}
			s = setHolders(s);
			WhenPluginUpdateTextEvent event = new WhenPluginUpdateTextEvent(p, s);
			Bukkit.getPluginManager().callEvent(event);
			String[] ts = splitString(event.getText());
			setPrefix(t, ts[0]);
			setSuffix(t, ts[1]);
		}
	}

	public void setPrefix(Team t, String string){
		if(string.length() > 16){
			t.setPrefix(string.substring(0, 16));
			return;
		}
		t.setPrefix(string);
	}

	public void setSuffix(Team t, String string){
		if(string.length() > 16){
			t.setSuffix(maxChars(16, string));
		}else{
			t.setSuffix(string.substring(0, string.length()));
		}
	}

	public String setHolders(String s){
		s = s.replace("{PLAYER}", p.getName()).replace("{ONLINE}", Bukkit.getOnlinePlayers().size() + "").replace("{TIME}", p.getWorld().getTime() + "").replace("{RND}", new Random().nextInt(999999) + "");
		s = PlaceholderAPI.setPlaceholders(p, s);
		s = ChatColor.translateAlternateColorCodes('&', s);
		return s;
	}

	public void updateTitle() {
		if(ch){
			return;
		}
		if(titleindex > (title.size() - 1)){
			titleindex = 0;
		}
		score.setDisplayName(maxChars(32, PlaceholderAPI.setPlaceholders(p, title.get(titleindex))));
		titleindex++;
	}

	public String maxChars(int characters, String string){
		if(ChatColor.translateAlternateColorCodes('&', string).length() > characters){
			return string.substring(0, characters);
		}
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public void remove(){
		stopTasks();
		QuickBoard.instance.boards.remove(p);
		QuickBoard.instance.allboards.remove(this);
		p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
	}

	private void stopTasks(){
		Bukkit.getScheduler().cancelTask(titleTask);
		Bukkit.getScheduler().cancelTask(textTask);
		
		for(int i : tasks){
			Bukkit.getScheduler().cancelTask(i);
		}
	}

	@SuppressWarnings("deprecation")
	public void updater(){
		titleTask = Bukkit.getScheduler().scheduleAsyncRepeatingTask(QuickBoard.instance, new Runnable(){
			public void run(){
				updateTitle();
			}
		}, 0, updateTitle);

		textTask = Bukkit.getScheduler().scheduleAsyncRepeatingTask(QuickBoard.instance, new Runnable(){
			public void run(){
				updateText();
			}
		}, 0, updateText);
		
		for(final String s : info.changeText.keySet()){
			int inter = info.changeTextInterval.get(s);
			
			int task = Bukkit.getScheduler().scheduleAsyncRepeatingTask(QuickBoard.instance, new Runnable(){
				public void run(){
					List<String> text = info.changeText.get(s);
					chanTextInt.put(s, chanTextInt.get(s) + 1);
					
					if(chanTextInt.get(s) >= text.size()){
						chanTextInt.put(s, 0);
					}
					int ta = chanTextInt.get(s);
					chanText.put(s, text.get(ta));
				}
			}, 0, inter);
			
			tasks.add(task);
		}
	}

	public void createNew(List<String> text, List<String> title, int updateTitle, int updateText){
		ch = true;
		stopTasks();
		removeAll();
		c();
		this.list = text;
		this.title = title;
		this.updateText = updateText;
		this.updateTitle = updateTitle;
		titleindex = this.title.size();

		score = board.getObjective("score");

		score.setDisplaySlot(DisplaySlot.SIDEBAR);

		score.setDisplayName(this.title.get(0));

		setUpText(text);

		ch = false;
		updater();
	}

	private void removeAll(){
		chlist.clear();
		score = null;
		titleindex = 0;
		index = 15;
		lot.clear();
		teams.clear();

		for(Team t : board.getTeams()){
			t.unregister();
		}

		for(String s : new ArrayList<>(board.getEntries())){
			board.resetScores(s);
		}
	}

	private String getResult(boolean BOLD, boolean ITALIC, boolean MAGIC, boolean STRIKETHROUGH, boolean UNDERLINE, ChatColor color){
		return ((color != null) && (!color.equals(ChatColor.WHITE)) ? color : "") + "" + (BOLD ? ChatColor.BOLD : "") + (ITALIC ? ChatColor.ITALIC : "") + (MAGIC ? ChatColor.MAGIC : "") + (STRIKETHROUGH ? ChatColor.STRIKETHROUGH : "") + (UNDERLINE ? ChatColor.UNDERLINE : "");
	}

	private String[] splitString(String string){
		StringBuilder prefix = new StringBuilder(string.substring(0, string.length() >= 16 ? 16 : string.length()));
		StringBuilder suffix = new StringBuilder(string.length() > 16 ? string.substring(16) : "");
		if (prefix.toString().length() > 1 && prefix.charAt(prefix.length() - 1) == '§')
		{
			prefix.deleteCharAt(prefix.length() - 1);
			suffix.insert(0, '§');
		}
		int length = prefix.length();
		boolean PASSED, UNDERLINE, STRIKETHROUGH, MAGIC, ITALIC;
		boolean BOLD = ITALIC = MAGIC = STRIKETHROUGH = UNDERLINE = PASSED = false;
		ChatColor textColor = null;
		for (int index = length - 1; index > -1; index--){
			char section = prefix.charAt(index);
			if ((section == '§') && (index < prefix.length() - 1)){
				char c = prefix.charAt(index + 1);
				ChatColor color = ChatColor.getByChar(c);
				if (color != null){
					if (color.equals(ChatColor.RESET)) {
						break;
					}
					if ((textColor == null) && (color.isFormat())){
						if ((color.equals(ChatColor.BOLD)) && (!BOLD)) {
							BOLD = true;
						} else if ((color.equals(ChatColor.ITALIC)) && (!ITALIC)) {
							ITALIC = true;
						} else if ((color.equals(ChatColor.MAGIC)) && (!MAGIC)) {
							MAGIC = true;
						} else if ((color.equals(ChatColor.STRIKETHROUGH)) && (!STRIKETHROUGH)) {
							STRIKETHROUGH = true;
						} else if ((color.equals(ChatColor.UNDERLINE)) && (!UNDERLINE)) {
							UNDERLINE = true;
						}
					}else if ((textColor == null) && (color.isColor())) {
						textColor = color;
					}
				}
			}else if ((index > 0) && (!PASSED)){
				char c = prefix.charAt(index);
				char c1 = prefix.charAt(index - 1);
				if ((c != '§') && (c1 != '§') && (c != ' ')) {
					PASSED = true;
				}
			}
			if ((!PASSED) && (prefix.charAt(index) != ' ')) {
				prefix.deleteCharAt(index);
			}
			if (textColor != null) {
				break;
			}
		}
		String result = suffix.toString().isEmpty() ? "" : getResult(BOLD, ITALIC, MAGIC, STRIKETHROUGH, UNDERLINE, textColor);
		if ((!suffix.toString().isEmpty()) && (!suffix.toString().startsWith("§"))) {
			suffix.insert(0, result);
		}
		return new String[] { prefix.toString().length() > 16 ? prefix.toString().substring(0, 16) : prefix.toString(), suffix.toString().length() > 16 ? suffix.toString().substring(0, 16) : suffix.toString() };
	}
}
