package me.tade.quickboard;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.CopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import me.tade.quickboard.cmds.QuickBoardCommand;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class QuickBoard extends JavaPlugin implements Listener {

	public HashMap<Player, PlayerBoard> boards = new HashMap<>();

	public List<PlayerBoard> allboards = new ArrayList<>();
	public HashMap<String, ScoreboardInfo> info = new HashMap<>();

	public static QuickBoard instance;

	public int texttask;
	public int titletask;

	@Override
	public void onEnable(){
		instance = this;
		if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") == null){
			Bukkit.getLogger().info("------------------------------");
			Bukkit.getLogger().info("          QuickBoard          ");
			Bukkit.getLogger().info("ERROR: Plugin 'PlaceholderAPI' not found");
			Bukkit.getLogger().info("Disabling...");
			Bukkit.getLogger().info("          QuickBoard          ");
			Bukkit.getLogger().info("------------------------------");
			this.getPluginLoader().disablePlugin(this);
			return;
		}
		try {
			new Metrics(this).start();
		} catch (IOException e) {
			getLogger().log(Level.WARNING, "An error was occured while enabling metrics! It is error of http://www.mcstats.org/");
			e.printStackTrace();
			getLogger().log(Level.WARNING, "An error was occured while enabling metrics! It is error of http://www.mcstats.org/");
		}
		Bukkit.getLogger().info("------------------------------");
		Bukkit.getLogger().info("          QuickBoard          ");
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getLogger().info("Listeners registered");

		Bukkit.getLogger().info("Loading scoreboards");

		File fo = new File(getDataFolder().getAbsolutePath() + "/scoreboards");
		if(!fo.exists()){
			fo.mkdir(); 	
			InputStream str = getResource("scoreboard.default.yml");
			try {
				copy(str, fo);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} 
		loadScoreboards(fo);
		Bukkit.getLogger().info("Scoreboards loaded");

		getConfig().options().copyDefaults(true);
		saveConfig();
		Bukkit.getLogger().info("Config registered");

		Bukkit.getLogger().info("Update Tasks started");

		try{
			getCommand("quickboard").setExecutor(new QuickBoardCommand());
			Bukkit.getLogger().info("Command 'quickboard' enabled");
		}catch(NullPointerException ex){
			Bukkit.getLogger().info("Command 'quickboard' disabled, error was occurred");
		}
		Bukkit.getLogger().info("          QuickBoard          ");
		Bukkit.getLogger().info("------------------------------");

		for(Player p : Bukkit.getOnlinePlayers()){
			if(getConfig().getBoolean("scoreboard.onjoin.use")){
				for(String s : info.keySet()){
					ScoreboardInfo in = info.get(s);
					if(in.enabledWorlds != null && in.enabledWorlds.contains(p.getWorld().getName())){
						if(p.hasPermission(s)){
							if(boards.containsKey(p)){
								boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
							}else{
								new PlayerBoard(p, info.get(s));
							}
							break;
						}
					}
				}
			}
		}
	}

	@Override
	public void onDisable(){
		for(Player p : Bukkit.getOnlinePlayers()){
			p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		if(getConfig().getBoolean("scoreboard.onjoin.use")){
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
				public void run(){
					if(p.isOnline()){
						for(String s : info.keySet()){
							ScoreboardInfo in = info.get(s);
							if(in.enabledWorlds != null && in.enabledWorlds.contains(p.getWorld().getName())){
								if(p.hasPermission(s)){
									if(boards.containsKey(p)){
										boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
									}else{
										new PlayerBoard(p, info.get(s));
									}
									break;
								}
							}
						}
					}
				}
			}, 10L);
		}
	}

	@EventHandler
	public void onJoin(PlayerTeleportEvent e){
		final Player p = e.getPlayer();

		if(!e.getFrom().getWorld().getName().equalsIgnoreCase(e.getTo().getWorld().getName())){
			if(getConfig().getBoolean("scoreboard.onjoin.use")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					public void run(){
						if(p.isOnline()){
							for(String s : info.keySet()){
								ScoreboardInfo in = info.get(s);
								if(in.enabledWorlds != null && in.enabledWorlds.contains(p.getWorld().getName())){
									if(p.hasPermission(s)){
										if(boards.containsKey(p)){
											boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
										}else{
											new PlayerBoard(p, info.get(s));
										}
										break;
									}
								}
							}
						}
					}
				}, 10L);
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerRespawnEvent e){
		final Player p = e.getPlayer();

		if(getConfig().getBoolean("scoreboard.onjoin.use")){
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
				public void run(){
					if(p.isOnline()){
						for(String s : info.keySet()){
							ScoreboardInfo in = info.get(s);
							if(in.enabledWorlds != null && in.enabledWorlds.contains(p.getWorld().getName())){
								if(p.hasPermission(s)){
									if(boards.containsKey(p)){
										boards.get(p).createNew(in.text, in.title, in.titleUpdate, in.textUpdate);
									}else{
										new PlayerBoard(p, info.get(s));
									}
									break;
								}
							}
						}	
					}
				}
			}, 10L);
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		if(boards.containsKey(e.getPlayer())){
			boards.get(e.getPlayer()).remove();
		}
	}


	public void loadScoreboards(File fo){
		if(fo.listFiles() == null){
			return;
		}

		if(Arrays.asList(fo.listFiles()).isEmpty()){
			return;
		}

		for(File f : fo.listFiles()){
			if(f.getName().endsWith(".yml")){
				String perm = f.getName().replace(".yml", "");
				YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
				ScoreboardInfo info = new ScoreboardInfo(cfg, perm);
				this.info.put(perm, info);
				Bukkit.getLogger().info("Loaded '" + f.getName() + "' with permission '" + perm + "'");
			}else{
				Bukkit.getLogger().warning("File '" + f.getName() + "' is not accepted! Accepted only .yml files");
			}
		}
	}

	public void copy(InputStream src, File dst) throws IOException {
		InputStream in = src;
		try {
			OutputStream out = new FileOutputStream(dst);
			try {
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
	}
}
